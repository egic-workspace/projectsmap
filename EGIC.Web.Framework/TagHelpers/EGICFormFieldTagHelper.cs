﻿using EGIC.Web.Framework.Extensions;
using EGIC.Web.Framework.Mvc.ModelBinding;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Net;

namespace EGIC.Web.Framework.TagHelpers
{
    /// <summary>
    /// egic-label tag helper
    /// </summary>
    [HtmlTargetElement("egic-form-field", Attributes = ForAttributeName, TagStructure = TagStructure.WithoutEndTag)]
    public class EGICFormFieldTagHelper : TagHelper
    {
        private const string ForAttributeName = "asp-for";
        private const string DisplayHintAttributeName = "asp-display-hint";
        private const string DisabledAttributeName = "asp-disabled";
        private const string MultipleAttributeName = "asp-multiple";
        private const string RequiredAttributeName = "asp-required";
        private const string RenderFormControlClassAttributeName = "asp-render-form-control-class";
        private const string TemplateAttributeName = "asp-template";
        private const string PostfixAttributeName = "asp-postfix";
        private const string NameAttributeName = "asp-for-name";
        private const string ItemsAttributeName = "asp-items";
        private const string InputTypeAttributeName = "asp-input-type";

        /// <summary>
        /// HtmlGenerator
        /// </summary>
        protected IHtmlGenerator Generator { get; set; }

        /// <summary>
        /// HtmlHelper
        /// </summary>
        private readonly IHtmlHelper _htmlHelper;

        /// <summary>
        /// An expression to be evaluated against the current model
        /// </summary>
        [HtmlAttributeName(ForAttributeName)]
        public ModelExpression For { get; set; }
        
        /// <summary>
        /// Name for a dropdown list
        /// </summary>
        [HtmlAttributeName(NameAttributeName)]
        public string Name { get; set; }

        /// <summary>
        /// Name for a dropdown list
        /// </summary>
        [HtmlAttributeName(InputTypeAttributeName)]
        public InputTypes InputType { get; set; }

        /// <summary>
        /// Items for a dropdown list
        /// </summary>
        [HtmlAttributeName(ItemsAttributeName)]
        public IEnumerable<SelectListItem> Items { set; get; } = new List<SelectListItem>();

        /// <summary>
        /// Indicates whether the hint should be displayed
        /// </summary>
        [HtmlAttributeName(DisplayHintAttributeName)]
        public bool DisplayHint { get; set; } = true;

        /// <summary>
        /// Indicates whether the input is multiple
        /// </summary>
        [HtmlAttributeName(MultipleAttributeName)]
        public string IsMultiple { set; get; }

        /// <summary>
        /// ViewContext
        /// </summary>
        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        /// <summary>
        /// Indicates whether the field is disabled
        /// </summary>
        [HtmlAttributeName(DisabledAttributeName)]
        public string IsDisabled { set; get; }

        /// <summary>
        /// Indicates whether the field is required
        /// </summary>
        [HtmlAttributeName(RequiredAttributeName)]
        public string IsRequired { set; get; }

        /// <summary>
        /// Indicates whether the "form-control" class shold be added to the input
        /// </summary>
        [HtmlAttributeName(RenderFormControlClassAttributeName)]
        public string RenderFormControlClass { set; get; }

        /// <summary>
        /// Editor template for the field
        /// </summary>
        [HtmlAttributeName(TemplateAttributeName)]
        public string Template { set; get; }

        /// <summary>
        /// Postfix
        /// </summary>
        [HtmlAttributeName(PostfixAttributeName)]
        public string Postfix { set; get; }

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="generator">HTML generator</param>
        /// <param name="workContext">Work context</param>
        /// <param name="localizationService">Localization service</param>
        public EGICFormFieldTagHelper(IHtmlGenerator generator, IHtmlHelper htmlHelper)
        {
            Generator = generator;
            _htmlHelper = htmlHelper;
        }

        /// <summary>
        /// Process
        /// </summary>
        /// <param name="context">Context</param>
        /// <param name="output">Output</param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null)
            {
                throw new ArgumentNullException(nameof(output));
            }

            /// left div (label container)
            TagBuilder labelContainerDiv = new TagBuilder("div");
            labelContainerDiv.AddCssClass("col-md-3");

            // label wrapper
            var labelWrapper = new TagBuilder("div");
            labelWrapper.AddCssClass("label-wrapper");

            #region generate label

            var tagBuilder = Generator.GenerateLabel(ViewContext, For.ModelExplorer, For.Name, null, new { @class = "control-label" });
            if (tagBuilder != null)
            {
                // add label
                labelWrapper.InnerHtml.AppendHtml(tagBuilder);

                // add hint
                if (For.Metadata.AdditionalValues.TryGetValue("EGICResourceDisplayNameAttribute", out object value))
                {
                    var resourceDisplayName = value as EGICResourceDisplayNameAttribute;
                    if (resourceDisplayName != null && DisplayHint)
                    {

                        var hintResource = resourceDisplayName.ResourceKey + ".Hint";

                        if (!string.IsNullOrEmpty(hintResource))
                        {
                            var hintContent = $"<div title='{WebUtility.HtmlEncode(hintResource)}' class='ico-help'><i class='fa fa-question-circle'></i></div>";
                            labelWrapper.InnerHtml.AppendHtml(hintContent);
                        }
                    }
                }
            }

            #endregion

            // add label wrapper into label container
            labelContainerDiv.InnerHtml.AppendHtml(labelWrapper);


            /// right div (editor container)
            TagBuilder editorContainerDiv = new TagBuilder("div");
            editorContainerDiv.AddCssClass("col-md-9");

            //contextualize IHtmlHelper
            var viewContextAware = _htmlHelper as IViewContextAware;
            viewContextAware?.Contextualize(ViewContext);

            var htmlAttributes = new Dictionary<string, object>();

            //get htmlAttributes object
            var attributes = context.AllAttributes;
            foreach (var attribute in attributes)
            {
                if (!attribute.Name.Equals(ForAttributeName) &&
                    !attribute.Name.Equals(NameAttributeName) &&
                    !attribute.Name.Equals(ItemsAttributeName) &&
                    !attribute.Name.Equals(MultipleAttributeName) &&
                    !attribute.Name.Equals("id",StringComparison.OrdinalIgnoreCase) &&
                    !attribute.Name.Equals(RequiredAttributeName))
                {
                    htmlAttributes.Add(attribute.Name, attribute.Value);
                }
            }

            //add form-control class
            bool.TryParse(RenderFormControlClass, out bool renderFormControlClass);
            if (string.IsNullOrEmpty(RenderFormControlClass) && For.Metadata.ModelType.Name.Equals("String") || renderFormControlClass)
                if (!htmlAttributes.ContainsKey("class"))
                    htmlAttributes.Add("class", "form-control");
                else
                    htmlAttributes["class"] += " form-control ";

            // disabled attribute
            bool.TryParse(IsDisabled, out bool disabled);
            if (disabled && !htmlAttributes.ContainsKey("disabled"))
                htmlAttributes.Add("disabled", "disabled");

            // generate input
            IHtmlContent htmlOutput;
            if (InputType == InputTypes.Select)
            {
                #region generate select

                var tagName = For != null ? For.Name : Name;
                bool.TryParse(IsMultiple, out bool multiple);
                if (multiple)
                {
                    htmlOutput = _htmlHelper.Editor(tagName, "MultiSelect", new { htmlAttributes, SelectList = Items });
                }
                else
                {
                    if (htmlAttributes.ContainsKey("class"))
                        htmlAttributes["class"] += " form-control btn-sm";
                    else
                        htmlAttributes.Add("class", "form-control btn-sm");
                    htmlOutput = _htmlHelper.DropDownList(tagName, Items, htmlAttributes);
                }

                editorContainerDiv.InnerHtml.AppendHtml($"<span id=\"loading-progress-{For.Name}\" style=\"display: none;\" class=\"please-wait\">نرجو الإنتظار...</span>");

                #endregion
            }
            else if (InputType == InputTypes.TextArea)
            {
                //additional parameters
                int rowsNumber = output.Attributes.ContainsName("rows") ? (int)output.Attributes["rows"].Value : 4;
                var colsNumber = output.Attributes.ContainsName("cols") ? (int)output.Attributes["cols"].Value : 32;
                var textArea = Generator.GenerateTextArea(ViewContext, For.ModelExplorer, For.Name, rowsNumber, colsNumber, new { @class = "form-control" });
                htmlOutput = textArea;
            }
            else
            {
                #region generate editor

                //we have to invoke strong typed "EditorFor" method of HtmlHelper<TModel>
                //but we cannot do it because we don't have access to Expression<Func<TModel, TValue>>
                //more info at https://github.com/aspnet/Mvc/blob/dev/src/Microsoft.AspNetCore.Mvc.ViewFeatures/ViewFeatures/HtmlHelperOfT.cs

                //so we manually invoke implementation of "GenerateEditor" method of HtmlHelper
                //more info at https://github.com/aspnet/Mvc/blob/dev/src/Microsoft.AspNetCore.Mvc.ViewFeatures/ViewFeatures/HtmlHelper.cs

                //little workaround here. we need to access private properties of HtmlHelper
                //just ensure that they are not renamed by asp.net core team in future versions
                var viewEngine = CommonHelper.GetPrivateFieldValue(_htmlHelper, "_viewEngine") as IViewEngine;
                var bufferScope = CommonHelper.GetPrivateFieldValue(_htmlHelper, "_bufferScope") as IViewBufferScope;
                var templateBuilder = new TemplateBuilder(
                    viewEngine,
                    bufferScope,
                    _htmlHelper.ViewContext,
                    _htmlHelper.ViewData,
                    For.ModelExplorer,
                    For.Name,
                    Template,
                    readOnly: false,
                    additionalViewData: new { htmlAttributes, postfix = this.Postfix });

                htmlOutput = templateBuilder.Build();

                #endregion
            }

            //required asterisk
            bool.TryParse(IsRequired, out bool required);
            if (required)
            {
                // group required
                var inputGroupRequired = new TagBuilder("div");
                inputGroupRequired.AddCssClass("input-group input-group-required");

                // group btn (*) 
                var inputGroupBtn = new TagBuilder("div");
                inputGroupBtn.AddCssClass("input-group-btn");
                inputGroupBtn.InnerHtml.AppendHtml("<span class=\"required\">*</span>");

                // append htmlOutput and inputGroupBtn into inputGroupRequired
                inputGroupRequired.InnerHtml.AppendHtml(htmlOutput);

                inputGroupRequired.InnerHtml.AppendHtml(inputGroupBtn);

                // put inputGroupRequired into editorContainerDiv
                editorContainerDiv.InnerHtml.AppendHtml(inputGroupRequired);
            }
            else
                editorContainerDiv.InnerHtml.AppendHtml(htmlOutput);


            // validation field
            editorContainerDiv.InnerHtml.AppendHtml($"<span class=\"field-validation-valid\" data-valmsg-for=\"{For.Name}\" data-valmsg-replace=\"true\"></span>");

            //output div (output)
            output.TagName = "div";
            output.TagMode = TagMode.StartTagAndEndTag;

            //merge classes
            var classValue = output.Attributes.ContainsName("class")
                                ? $"form-group {output.Attributes["class"].Value}"
                                : "form-group";
            output.Attributes.SetAttribute("class", classValue);

            // add label container div
            output.Content.AppendHtml(labelContainerDiv);

            // add editor container div
            output.Content.AppendHtml(editorContainerDiv);
        }
    }

    public enum InputTypes
    {
        Editor =1,
        Select,
        TextArea
    }
}