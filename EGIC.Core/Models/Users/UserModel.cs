﻿using EGIC.Core.Domain.Users;
using EGIC.Web.Framework.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;

namespace EGIC.Core.Models.Users
{
    public partial class UserModel : User
    {
        public UserModel()
        {
        }

        public UserModel(User user):this()
        {
            Code = user.Code;
            FullName = user.FullName;
            Username = user.Username;
            Password = user.Password;
        }


    }
}