﻿using EGIC.Core.Domain.ProjectsMap;
using EGIC.Web.Framework.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EGIC.Core.Models.ProjectsMap
{
    public class RepTrackModel
    {
        public RepTrackModel()
        {
            Reps = new List<SelectListItem>();
        }

        [UIHint("DateNullable")]
        [EGICResourceDisplayName("Select Date")]
        public DateTime? SelectDate { get; set; }

        [EGICResourceDisplayName("Rep Username")]
        public int RepUsername { get; set; }
        public List<SelectListItem> Reps { get; set; }

    }
}
