﻿using EGIC.Core.Domain.ProjectsMap;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace EGIC.Core.Models.ProjectsMap
{
    public partial class ProjectsMapModel 
    {
        public ProjectsMapModel()
        {
            ProjectTypes = new List<SelectListItem>();
        }
        public IList<SelectListItem> ProjectTypes { get; set; }

    }
}