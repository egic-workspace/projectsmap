﻿using EGIC.Core.Models.ProjectsMap;
using EGIC.Web.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace EGIC.Core.Domain.ProjectsMap
{
    public partial class Project : BaseEGICEntity
    {
        #region CTOR

        public Project() : base()
        {
        }

        public Project(DataRow row) : base(row) 
        {
            try
            {
                if (int.TryParse(row[CUST_TYPE].ToString(), out int customerType))
                    CustomerType = customerType;
                
                if (int.TryParse(row[CUST_CODE].ToString(), out int customerCode))
                    CustomerCode = customerCode;

                if (int.TryParse(row[PROJ_CODE].ToString(), out int projectCode))
                    ProjectCode = projectCode;
                CustomerName = row[CUST_NAME].ToString();
                Longtude = row[LONGTUDE].ToString();
                Latitude = row[LATITUDE].ToString();
            }
            catch { }
        }

        #endregion

        #region Properties


        [EGICResourceDisplayName("Longtude")]
        public string Longtude { get; set; }

        [EGICResourceDisplayName("Latitude")]
        public string Latitude { get; set; }

        [EGICResourceDisplayName("Customer Name")]
        public string CustomerName { get; set; }

        [EGICResourceDisplayName("Project Code")]
        public int ProjectCode { get; set; }

        [EGICResourceDisplayName("Customer Type")]
        public int CustomerType { get; set; }

        [EGICResourceDisplayName("Customer Code")]
        public int CustomerCode { get; set; }

        #endregion
    }
}