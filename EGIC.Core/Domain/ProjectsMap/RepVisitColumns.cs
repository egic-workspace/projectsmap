﻿namespace EGIC.Core.Domain.ProjectsMap
{
    public partial class RepVisit
    {
        public const string VISIT_ORDER = nameof(VISIT_ORDER);
        public const string CUST_NAME = nameof(CUST_NAME);
        public const string VISIT_TIME = nameof(VISIT_TIME);
        public const string LONGTUDE = nameof(LONGTUDE);
        public const string LATITUDE = nameof(LATITUDE);
    }
}