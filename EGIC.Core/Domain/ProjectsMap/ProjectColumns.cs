﻿namespace EGIC.Core.Domain.ProjectsMap
{
    public partial class Project
    {
        public const string PROJ_CODE = nameof(PROJ_CODE);
        public const string PROJ_DESC = nameof(PROJ_DESC);
        public const string CUST_TYPE = nameof(CUST_TYPE);
        public const string CUST_CODE = nameof(CUST_CODE);
        public const string CUST_NAME = nameof(CUST_NAME);
        public const string LONGTUDE = nameof(LONGTUDE);
        public const string LATITUDE = nameof(LATITUDE);
    }
}