﻿using EGIC.Web.Framework.Mvc.ModelBinding;
using System.Data;

namespace EGIC.Core.Domain.ProjectsMap
{
    public partial class RepVisit : BaseEGICEntity
    {
        #region CTOR

        public RepVisit() : base()
        {
        }

        public RepVisit(DataRow row) : base(row) 
        {
            try
            {
                if (int.TryParse(row[VISIT_ORDER].ToString(), out int order))
                    Order = order;
                VisitTime = row[VISIT_TIME].ToString();
                CustomerName = row[CUST_NAME].ToString();
                Longtude = row[LONGTUDE].ToString();
                Latitude = row[LATITUDE].ToString();
            }
            catch { }
        }

        #endregion

        #region Properties


        [EGICResourceDisplayName("Order")]
        public int Order { get; set; }

        [EGICResourceDisplayName("Longtude")]
        public string Longtude { get; set; }

        [EGICResourceDisplayName("Latitude")]
        public string Latitude { get; set; }

        [EGICResourceDisplayName("Customer Name")]
        public string CustomerName { get; set; }

        [EGICResourceDisplayName("Visit Time")]
        public string VisitTime { get; set; }

        #endregion
    }
}