﻿using EGIC.Core.Domain.Common;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace ProjectsMap.Services.Common
{
    public interface ICommonService
    {
        List<AppSetting> GetAppSettings();

        List<SelectListItem> GetAllReps(string selectedRepUsername = "");

        List<SelectListItem> GetAllProjectTypes();

    }
}