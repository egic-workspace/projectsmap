﻿using EGIC.Core.Domain.Common;
using EGIC.Data.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace ProjectsMap.Services.Common
{
    public class CommonService : ICommonService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;

        #endregion

        #region CTOR

        public CommonService(IServiceProvider service)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
        }

        #endregion

        #region Methods


        /// <summary>
        /// Gets or sets the current user permissions
        /// </summary>
        public virtual List<AppSetting> GetAppSettings()
        {
            string sql = "SELECT CODE,SETTING_NAME,SETTING_VALUE FROM APPS_SETTINGS WHERE APP_NAME = 'ProjectsMap'";
            var data = _oracleAcess.ExecuteDataSet(sql);

            if (data.Tables[0].Rows.Count == 0)
                return new List<AppSetting>();

            List<AppSetting> _appSettings = new List<AppSetting>();
            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                _appSettings.Add(new AppSetting(data.Tables[0].Rows[i]));

            return _appSettings;
        }

        public virtual List<SelectListItem> GetAllReps(string selectedRepUsername = "")
        {
            string sql = "SELECT USER_NAME as VALUE,ARABIC_NAME as TEXT FROM USERS WHERE USER_TYPE = 'PROJ' ";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem> { new SelectListItem { Text = "اختر المندوب", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++) {
                var row = data.Tables[0].Rows[i];
                string username = row["VALUE"].ToString();
                list.Add(new SelectListItem() { Text = row["TEXT"].ToString(), Value = username, Selected = selectedRepUsername == username });
            }
            return list;
        }
        
        public virtual List<SelectListItem> GetAllProjectTypes()
        {
            string sql = "SELECT CODE as VALUE,NAME as TEXT FROM PS_CUST_TYPES ";
            var data = _oracleAcess.ExecuteDataSet(sql);

            List<SelectListItem> list = new List<SelectListItem>();// { new SelectListItem { Text = "اختر نوع المشروع", Value = "" } };
            if (data.Tables[0].Rows.Count == 0)
                return list;

            for (int i = 0; i < data.Tables[0].Rows.Count; i++) {
                var row = data.Tables[0].Rows[i];
                string username = row["VALUE"].ToString();
                list.Add(new SelectListItem() { Text = row["TEXT"].ToString(), Value = username});
            }
            return list;
        }

        #endregion
    }
}