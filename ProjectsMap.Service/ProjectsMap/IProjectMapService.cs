﻿using EGIC.Core.Domain.ProjectsMap;
using EGIC.Web.Framework;
using System;
using System.Collections.Generic;

namespace ProjectsMap.Services.ProjectsMap
{
    public interface IProjectMapService
    {
        IList<Project> GetAllProjects();

        IList<RepVisit> GetRepVisits(DateTime? date, string username);

    }
}