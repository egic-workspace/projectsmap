﻿using EGIC.Core.Domain.ProjectsMap;
using EGIC.Data.DAL;
using EGIC.Web.Framework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ProjectsMap.Services.ProjectsMap
{
    public class ProjectMapService : IProjectMapService
    {
        #region Fields

        private readonly IOracleAcess _oracleAcess;
        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region CTOR

        public ProjectMapService(IServiceProvider service, IHostingEnvironment hostingEnvironment)
        {
            _oracleAcess = service.GetService<IOracleAcess>();
            _hostingEnvironment = hostingEnvironment;
        }

        #endregion

        #region Methods

        public virtual IList<Project> GetAllProjects()
        {
            var entities = new List<Project>();
            string sqlQuery = 
                $@"SELECT 
                        P.PROJ_CODE, 
                        P.PROJ_DESC, 
                        C.CUST_TYPE, 
                        C.CUST_CODE, 
                        C.CUST_NAME, 
                        P.LONGTUDE, 
                        P.LATITUDE 
                    FROM 
                        PS_PROJECTS P, PS_CUSTOMERS C
                    WHERE 
                        P.PROJ_CUST_CODE = C.CUST_CODE
                        AND P.LONGTUDE IS NOT NULL
                        AND P.LATITUDE IS NOT NULL";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                entities.Add(new Project(data.Tables[0].Rows[i]));

            return entities;
        }
        
        public virtual IList<RepVisit> GetRepVisits(DateTime? date,string username)
        {
            //07-MAY-2020 (date format)
            var entities = new List<RepVisit>();
            string sqlQuery = 
                $@"SELECT 
                        ROWNUM as VISIT_ORDER, 
                        C.CUST_NAME, 
                        TO_CHAR(V.VISIT_DATE,'HH24:MI:SS') VISIT_TIME, 
                        V.LONGTUDE, 
                        V.LATITUDE 
                    FROM 
                        PS_VISITS V, PS_CUSTOMERS C
                    WHERE 
                        V.CUST_CODE = C.CUST_CODE
                        AND VISIT_DATE between to_date('{date.Value.ToString(RepVisit.DATE_FORMAT_NET)}','{RepVisit.DATE_FORMAT_ORECAL}') and 
                                                to_date('{date.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString(RepVisit.DATE_FORMAT_NET)}','{RepVisit.DATE_FORMAT_ORECAL}') 
                        AND V.VISITOR_NAME = '{username}'
                        AND V.LONGTUDE IS NOT NULL
                        AND V.LATITUDE IS NOT NULL
                    ORDER BY V.VISIT_DATE";

            var data = _oracleAcess.ExecuteDataSet(sqlQuery);

            for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                entities.Add(new RepVisit(data.Tables[0].Rows[i]));

            return entities;
        }


        #endregion
    }
}