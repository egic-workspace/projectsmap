﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Data;
namespace EGIC.Data.DAL
{
    /// <summary>
    /// Summary description for OracleAcess
    /// </summary>
    public interface IOracleAcess
    {
        int ExecuteNonQuery(string sql);

        int ExecuteNonQuery(string sql, OracleParameter param);

        int ExecuteNonQuery(string sql, OracleParameter[] @params);

        void SaveData(DataTable dt, string Sql);

        void BeginTransaction();

        void Rollback();

        void Commit();

        object ExecuteScalar(string sql, OracleParameter[] @params);

        object ExecuteScalar(string sql);

        DataTable ExecuteDataTable(string sql);

        DataTable ExecuteDataTable(string sql, OracleParameter[] @params);

        DataSet ExecuteDataSet(string sql);

        int excute_PRC(string PRC, OracleParameter[] @params);
    }
}