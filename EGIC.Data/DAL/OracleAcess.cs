﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace EGIC.Data.DAL
{

    /// <summary>
    /// Summary description for OracleAcess
    /// </summary>
    public class OracleAcess : IOracleAcess
    {
        public static string connectionstring;
        private static OracleConnection oracleConnection;

        private bool WithTransaction;
        private OracleTransaction _transaction;
        private readonly IConfiguration _configuration;

        public OracleAcess(IConfiguration configuration)
        {
            _configuration = configuration;

            if (string.IsNullOrWhiteSpace(connectionstring))
                connectionstring = _configuration["ConnectionStrings:DefaultConnection"];
        }

        public OracleAcess(string ConnectionName, IConfiguration configuration) :
            this(configuration)
        {

            WithTransaction = false;
            if (!string.IsNullOrWhiteSpace(ConnectionName) && string.IsNullOrWhiteSpace(connectionstring))
                connectionstring = _configuration["ConnectionStrings:DefaultConnection"];

            if (oracleConnection == null)
                oracleConnection = new OracleConnection(connectionstring);
        }

        public OracleTransaction Transaction
        {
            get { return this._transaction; }
            set { this._transaction = value; }
        }
        public int ExecuteNonQuery(string sql)
        {
            sql = sql.Trim();
            oracleConnection = new OracleConnection(connectionstring);
            OracleCommand cmd = new OracleCommand(sql, oracleConnection);
            try
            {
                if (oracleConnection.State != ConnectionState.Open)
                {
                    oracleConnection.Open();
                }
                if ((WithTransaction))
                {
                    cmd.Transaction = Transaction;
                }

                bool returnCode = sql.EndsWith("into :code");
                if (returnCode)
                {
                    cmd.Parameters.Add(new OracleParameter
                    {
                        ParameterName = ":code",
                        OracleDbType = OracleDbType.Int64,
                        Direction = ParameterDirection.Output
                    });
                }
                int retval = cmd.ExecuteNonQuery();
                if (returnCode && int.TryParse(cmd.Parameters[":code"].Value.ToString(), out int outCode))
                    retval = outCode;

                if ((!WithTransaction))
                {
                    oracleConnection.Close();
                }
                return retval;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            finally
            {
                oracleConnection.Dispose();
            }
            return -1;
        }
        public int ExecuteNonQuery(string sql, OracleParameter param)
        {
            sql = sql.Trim();
            oracleConnection = new OracleConnection(connectionstring);
            OracleCommand cmd = new OracleCommand(sql, oracleConnection);
            try
            {
                if (oracleConnection.State != ConnectionState.Open)
                {
                    oracleConnection.Open();
                }
                cmd.Parameters.Add(param);
                if ((WithTransaction))
                {
                    cmd.Transaction = Transaction;
                }
                bool returnCode = sql.EndsWith("into :code");
                if (returnCode)
                {
                    cmd.Parameters.Add(new OracleParameter
                    {
                        ParameterName = ":code",
                        OracleDbType = OracleDbType.Int64,
                        Direction = ParameterDirection.Output
                    });
                }
                int retval = cmd.ExecuteNonQuery();
                if (returnCode && int.TryParse(cmd.Parameters[":code"].Value.ToString(), out int outCode))
                    retval = outCode;

                if ((!WithTransaction))
                {
                    oracleConnection.Close();
                }
                return retval;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            finally
            {
                oracleConnection.Close();
                oracleConnection.Dispose();
            }
            return -1;
        }

        public int ExecuteNonQuery(string sql, OracleParameter[] @params)
        {
            oracleConnection = new OracleConnection(connectionstring);
            OracleCommand cmd = new OracleCommand(sql, oracleConnection);
            try
            {
                for (int i = 0; i <= @params.Length - 1; i++)
                {
                    cmd.Parameters.Add(@params[i]);
                }

                if (oracleConnection.State != ConnectionState.Open)
                {
                    oracleConnection.Open();
                }
                if ((WithTransaction))
                {
                    cmd.Transaction = Transaction;
                }
                int retval = cmd.ExecuteNonQuery();
                if ((!WithTransaction))
                {
                    oracleConnection.Close();
                }
                return retval;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            finally
            {
                oracleConnection.Dispose();
            }
            return -1;
        }
        public void SaveData(DataTable dt, string sql)
        {
            OracleDataAdapter adp = new OracleDataAdapter(sql, connectionstring);
            OracleCommandBuilder cmAdp = new OracleCommandBuilder(adp);

            adp.InsertCommand = cmAdp.GetInsertCommand();
            adp.DeleteCommand = cmAdp.GetDeleteCommand();
            adp.UpdateCommand = cmAdp.GetUpdateCommand();
            adp.Update(dt);
        }
        public void BeginTransaction()
        {
            WithTransaction = true;
            if (oracleConnection.State != ConnectionState.Open)
            {
                oracleConnection.Open();
            }
            Transaction = oracleConnection.BeginTransaction();
        }
        public void Rollback()
        {
            WithTransaction = false;
            Transaction.Rollback();
            if (oracleConnection.State != ConnectionState.Closed)
            {
                oracleConnection.Close();
            }
        }
        public void Commit()
        {
            WithTransaction = false;
            Transaction.Commit();
            if (oracleConnection.State != ConnectionState.Closed)
            {
                oracleConnection.Close();
            }
        }
        public object ExecuteScalar(string sql, OracleParameter[] @params)
        {
            OracleConnection cnn = new OracleConnection(connectionstring);
            OracleCommand cmd = new OracleCommand(sql, cnn);
            try
            {
                for (int i = 0; i <= @params.Length - 1; i++)
                {
                    cmd.Parameters.Add(@params[i]);
                }

                if (cnn.State != ConnectionState.Open)
                {
                    cnn.Open();
                }
                if ((WithTransaction))
                {
                    cmd.Transaction = Transaction;
                }
                object retval = cmd.ExecuteScalar();
                if ((!WithTransaction))
                {
                    cnn.Close();
                }
                return retval;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            finally
            {
                cnn.Dispose();
            }
            return -1;
        }
        public object ExecuteScalar(string sql)
        {
            oracleConnection = new OracleConnection(connectionstring);
            OracleCommand cmd = new OracleCommand(sql, oracleConnection);
            try
            {
                if (oracleConnection.State != ConnectionState.Open)
                {
                    oracleConnection.Open();
                }
                if ((WithTransaction))
                {
                    cmd.Transaction = Transaction;
                }
                object retval = cmd.ExecuteScalar();
                if ((!WithTransaction))
                {
                    oracleConnection.Close();
                }
                return retval;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            finally
            {
                oracleConnection.Dispose();
            }
            return -1;
        }
        public DataTable ExecuteDataTable(string sql)
        {
            DataTable dt = new DataTable();
            OracleDataAdapter da = new OracleDataAdapter(sql, oracleConnection);
            da.Fill(dt);
            return dt;
        }
        public DataTable ExecuteDataTable(string sql, OracleParameter[] @params)
        {
            DataTable dt = new DataTable();
            OracleDataAdapter da = new OracleDataAdapter(sql, connectionstring);
            for (int i = 0; i <= @params.Length - 1; i++)
            {
                da.SelectCommand.Parameters.Add(@params[i]);
            }
            da.Fill(dt);
            return dt;
        }
        public DataSet ExecuteDataSet(string sql)
        {
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(sql, connectionstring);
            da.Fill(ds);

            return ds;
        }
        public int excute_PRC(string PRC, OracleParameter[] @params)
        {
            try
            {
                OracleCommand cmd = new OracleCommand();
                oracleConnection = new OracleConnection(connectionstring);
                cmd.Connection = oracleConnection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = PRC;

                for (int i = 0; i <= @params.Length - 1; i++)
                {
                    cmd.Parameters.Add(@params[i]);
                }

                if (oracleConnection.State != ConnectionState.Open)
                {
                    oracleConnection.Open();
                }
                if ((WithTransaction))
                {
                    cmd.Transaction = Transaction;
                }
                int retval = cmd.ExecuteNonQuery();
                if ((!WithTransaction))
                {
                    oracleConnection.Close();
                }
                return retval;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            finally
            {
                oracleConnection.Dispose();
            }
            return -1;
        }
    }
}