﻿using EGIC.Core.Domain.ProjectsMap;
using EGIC.Core.Models.ProjectsMap;
using EGIC.Web.Framework;
using EGIC.Web.Framework.Extensions;
using EGIC.Web.Framework.Kendoui;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using ProjectsMap.Services.Common;
using ProjectsMap.Services.ProjectsMap;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace ProjectsMap.Web.Controllers
{
    [Route("Projects")]
    public class ProjectsController : BaseController
    {

        #region Fields

        private readonly IProjectMapService _projectsMapService;
        private readonly ICommonService _commonService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;

        #endregion

        #region Ctor

        public ProjectsController(IServiceProvider service,
            IProjectMapService projectsMapService,
            ICommonService commonService, 
            IHostingEnvironment hostingEnvironment,
            IConfiguration config) : base(service)
        {
            _hostingEnvironment = hostingEnvironment;
            _projectsMapService = projectsMapService;
            _commonService = commonService;
            _config = config;
        }

        #endregion

        #region Actions

        [Route("Map")]
        public virtual IActionResult Index()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();

            var model = new ProjectsMapModel();
            model.ProjectTypes = _commonService.GetAllProjectTypes();

            return View(model);
        }

        [Route("ProjectsMap"), HttpPost]
        public virtual IActionResult ProjectsMap()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();

            var data = _projectsMapService.GetAllProjects();
            if (data.Count == 0)
                return ErrorForKendoGridJson("No projects found!");

            return Json(data);
        }
        [Route("RepTrack")]
        public virtual IActionResult RepTrack()
        {
            if (!_permissionService.Authorize())
                return AccessDeniedView();
            var model = new RepTrackModel();
            model.Reps = _commonService.GetAllReps();
            return View(model);
        }

        [Route("RepVisits"), HttpPost]
        public virtual IActionResult RepVisits(string username, int year, int month, int day)
        {
            if (!_permissionService.Authorize())
                return AccessDeniedKendoGridJson();

            var date = new DateTime(year, month, day);

            var data = _projectsMapService.GetRepVisits(date, username);
            return Json(new { success = true, data = data });
        }


        #endregion

    }
}