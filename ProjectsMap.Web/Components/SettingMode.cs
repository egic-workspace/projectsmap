﻿using Microsoft.AspNetCore.Mvc;
using EGIC.Core.Models.Settings;
using EGIC.Web.Framework.Components;

namespace EGIC.Web.Components
{
    public class SettingModeViewComponent : EGICViewComponent
    {

        public SettingModeViewComponent()
        {
        }

        public IViewComponentResult Invoke(string modeName = "settings-advanced-mode")
        {
            var model = new ModeModel
            {
                ModeName = modeName,
                Enabled = true
            };

            return View(model);
        }
    }
}